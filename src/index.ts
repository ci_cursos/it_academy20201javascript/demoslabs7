import fetch, {RequestInit} from 'node-fetch';

interface Post {
    id?: number,
    userId: number,
    title: string,
    body: string
}

async function main() {
    const urlBase = 'http://jsonplaceholder.typicode.com';
    try {
        //Realizar um GET
        /*
        let resposta = await fetch(`${urlBase}/posts`);
        if (resposta.ok) {
            let posts: Post[] = await resposta.json();
            posts.forEach(p => console.log(p.title));
        } else {
            console.log('GET falhou:');
            console.log(resposta.status);
        }
        */
        //Realizar um POST
        let post: Post = {
            userId: 1000,
            title: 'Um teste de post',
            body: 'Um corpo de exemplo'
        };
        let opcoes: RequestInit = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(post)
        };
        let resposta = await fetch(`${urlBase}/posts`, opcoes);
        if (resposta.ok) {
            let post = await resposta.json();
            console.log('Resposta:');
            console.log(post);
        } else {
            console.log('POST falhou:');
            console.log(resposta.status);
        }
    } catch (error) {
        console.log('Erro:');
        console.log(error);
    }
}

main();